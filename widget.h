﻿#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <atomic>
#include <QTextStream>

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

private slots:
    void on_addBtn_clicked();
    void on_delBtn_clicked();

    void on_startBtn_clicked();

    void on_stopBtn_clicked();

private:
    Ui::Widget *ui;
    std::atomic_int fileCount{0};
    std::atomic_int fileIndex{0};

    std::atomic_bool isStop{false};
    QVector<QString> fileVector;

signals:
    void convertFile(QString file);
    void foundFile();

private:
    void listFilesRecursively(const QString &path, QStringList extension);

    void changeFileCode(const QString &filePath, bool isAddBom);

};
#endif // WIDGET_H
